extends Reference
#http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
class_name GD_SDF


static func torus( p:Vector3,t:Vector2)->float:
	return -INF


static func sphere( p:Vector3, s:float)->float:
	return p.length()-s
	

static func box(  p:Vector3, b:Vector3 )->float:
	var q:Vector3 = p.abs() - b
	return Vector3(max(q.x,0),max(q.y,0),max(q.z,0)).length() + min(max(q.x,max(q.y,q.z)),0.0)


static func roundBox(p:Vector3,b:Vector3,r:float)->float:
	var q :Vector3 = p.abs() - b
	return Vector3(max(q.x,0),max(q.y,0),max(q.z,0)).length() + min(max(q.x,max(q.y,q.z)),0.0) - r


static func OPunion(d1:float,d2:float)->float:
	return min(d1,d2)


static func OPsubtraction(d1:float,d2:float)->float:
	return max(d1,-d2)


static func OPintersction(d1:float,d2:float)->float:
	return max(d1,d2)
