class_name SpawnPoint

var transform:Transform = Transform() setget setNull
var position:Vector3 setget setPosition,getPosition

func setNull(value):
	assert("cannot set this value!")



func getPosition():
	return transform.origin

func setPosition(value:Vector3):
	var up:= value.normalized()
	var facing:= Vector3(randf(),randf(),randf())
	facing = (facing - facing.project(up)).normalized()
	var left:= up.cross(facing)
	transform.origin = value
	transform.basis = Basis(facing,up,left)
