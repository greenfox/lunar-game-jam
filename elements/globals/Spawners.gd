#extends Resource

class_name Spawners

const turnFraction:float = 0.61803398874989484820459
export (int) var count:int = 2500
#export (float) var newScale = 100

var list = []

func _init():
	for i in count:
		var coord = nodeCoord(i)
		list.push_back(coord)

func pruneInvalidSpawnPoints(planet):
	for i in list:
		pass



func nodeCoord(spawnNumber)->Vector3:
	var t:float = spawnNumber / (count-1.0)
	var inclination:float = acos(1 - 2 * t)
	var azimuth:float = 2 * PI * turnFraction * spawnNumber

	var x:float = sin(inclination) * cos(azimuth)
	var z:float = sin(inclination) * sin(azimuth)
	var y:float = cos(inclination)

	return Vector3(x,y,z)
