extends VoxelStream

class_name ScriptedDoublePassStreamer

var voxelType = 1

func pass_one(out_buffer:VoxelBuffer, origin:Vector3, lod:int)->bool:
	printerr("ScriptedDoublePassStreamer.pass_one not defined")
	assert(false)
	return true

func pass_two(coord:Vector3) ->float:
	printerr("ScriptedDoublePassStreamer.pass_two not defined")
	assert(false)
	return 1.0

func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
	if self.pass_one(out_buffer,origin,lod) != true:
		return
		
	var scale = pow(2,lod)
	var dataSize = out_buffer.get_size()
	var realPos = Vector3.ZERO
	for x in dataSize.x:
		realPos.x = x*scale+origin.x
		for y in dataSize.y:
			realPos.y = y*scale+origin.y
			for z in dataSize.z:
				realPos.z = z*scale+origin.z
				var v = self.pass_two(realPos)
#				v = v/scale
				out_buffer.set_voxel_f(v,x,y,z,self.voxelType)

