tool
extends VoxelStream

class_name SDFThing

var voxel_type:int = 1


var radius = 2400

var bb = Vector3(radius,radius,radius)
var planetAABB = AABB(bb,2*bb)

func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
#	print(planetAABB)
	var scale = pow(2,lod)
	var chunkAABB = AABB(origin,Vector3.ONE*scale)

	if chunkAABB.intersects(planetAABB) or planetAABB.intersects(chunkAABB):
#	if not cunkAABB.intersects(planetAABB):
		out_buffer.full()
#		print('eject')
		return
	
	looper(out_buffer,origin,lod,self,"processPoint")
#	var s = out_buffer.get_size()
#	for x in s.x:
#		var xs = scale*x
#		for y in s.y:
#			var ys = scale*y
#			for z in s.z:
#				var zs = scale*z
#				var realPos = Vector3(xs,ys,zs) + origin
#				var v = processPoint(realPos)/scale
#				out_buffer.set_voxel_f(v,x,y,z,voxel_type)
					
					

func looper(buffer:VoxelBuffer,origin:Vector3,lod:int,object:Object,function:String):
	var scale:int = pow(2,lod)
	var s:Vector3 = buffer.get_size()
	for x in s.x:
		var xs = scale*x
		for y in s.y:
			var ys = scale*y
			for z in s.z:
				var zs = scale*z
				var realPos :=Vector3(xs,ys,zs) + origin
				var v = object.call(function,realPos)/scale
				buffer.set_voxel_f(v,x,y,z,voxel_type)


func processPoint(pos:Vector3)->float:
	var a = GD_SDF.sphere(pos,radius)
	return a
