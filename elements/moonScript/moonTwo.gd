tool
extends VoxelStream

class_name moonTwo

var voxel_type:int	= 1

#var radius:float = 64000
export (float) var radius = 640




func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
#	if lod != 0:
#		return
	var scale = pow(2,lod)
	
	var s = out_buffer.get_size()
	for x in s.x:
		var xs = scale*x
		for y in s.y:
			var ys = scale*y
			for z in s.z:
				var zs = scale*z
				var realPos = Vector3(xs,ys,zs) + origin
#				if realPos.length_squared() < r2:
				var v = realPos.length()
#					v = clamp(v,-1,1)
#				print(v)
				out_buffer.set_voxel_f(v,x,y,z,voxel_type)
#					out_buffer.fill(1,voxel_type)
					
					


