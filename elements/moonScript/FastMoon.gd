extends VoxelStream

class_name FastMoon

export (float) var radius:float = Globals.planetRadius
export (float) var crustScale = .25 setget setCrustSize

export (float) var threshold = .5

var crustSize:float #= radius*0 setget setCrustSize
var minAltitude:float# = radius-crustSize
var maxAltitude:float# = radius+crustSize

export (float) var noiseScale:float = 2 setget setNoiseScale
export (OpenSimplexNoise) var noise:OpenSimplexNoise
var trueNoiseScale = null

func _init():
	pass
func _ready():
	pass

func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
	var scale = pow(2,lod)
	var halfExtends = Vector3.ONE * scale * 8 #scale * EdgeSize / 2
	var center = origin + halfExtends
	var boxHalfExtends = halfExtends
	
	if GD_SDF.roundBox(-center,boxHalfExtends,maxAltitude) > 0:
		return #chunk is completely outside the planet
	if GD_SDF.roundBox(-center,boxHalfExtends,minAltitude-(2*boxHalfExtends.length())) < 0:
		out_buffer.fill_f(-1,1) #this chunk is completely INSIDE the planet
		return

	var dataSize = out_buffer.get_size()
	var realPos = Vector3.ZERO
	for x in dataSize.x:
		realPos.x = x*scale+origin.x
		for y in dataSize.y:
			realPos.y = y*scale+origin.y
			for z in dataSize.z:
				realPos.z = z*scale+origin.z
				var v = getPosition(realPos)
				v = v/scale
				out_buffer.set_voxel_f(v,x,y,z,1)
				
func getPosition(position:Vector3)->float:
	var v = GD_SDF.sphere(position,radius)
	var sphereNormal = position.normalized()*radius
	var noiseValue = noise.get_noise_3dv(sphereNormal/trueNoiseScale)
	var correctedNoise = min(ridge(noiseValue),threshold)#(min(threshold,ridge(noiseValue)) - threshold) * (1-threshold)
	v += correctedNoise * crustSize
#	print(noiseValue,":",correctedNoise)
	return v

func ridge(value):
	return pow(1-abs(value),4)

func errorChunk(out_buffer,origin,lod):
	var scale = pow(2,lod)
	var halfScale = scale/2
	var center = origin + Vector3(halfScale,halfScale,halfScale)
	print("=========ERROR========= at LOD:", lod)
	print("center:",center)
	print("scale:",scale)
	print("GD_SDF test:",GD_SDF.box(-center,Vector3(scale,scale,scale)))

func setCrustSize(value):
	crustScale = value
	crustSize = crustScale * radius
	minAltitude = radius-crustSize #idk if thiswill work 
	maxAltitude = radius#+crustSize


func setNoiseScale(value):
	noiseScale = value
	trueNoiseScale = (noiseScale * radius) / 1000  
