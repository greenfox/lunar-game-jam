tool
extends VoxelStream

class_name NoiseMoon

var voxel_type:int = 1


var radius = Globals.planetRadius
var noiseScale = radius*0.01

var bb = Vector3(radius,radius,radius)
var planetAABB = AABB(bb,2*bb)

var noise = OpenSimplexNoise.new()
func _ready():
	noise.seed = 0
	noise.octaves = 8
	noise.period = 20.0
	noise.persistence = 0.8

func emerge_block(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
#	print(planetAABB)
	var scale = pow(2,lod)
	var chunkAABB = AABB(origin,Vector3.ONE*scale)

	if chunkAABB.intersects(planetAABB) or planetAABB.intersects(chunkAABB):
#	if not cunkAABB.intersects(planetAABB):
		out_buffer.full()
#		print('eject')
		return
	
	
	var s = out_buffer.get_size()
	for x in s.x:
		var xs = scale*x
		for y in s.y:
			var ys = scale*y
			for z in s.z:
				var zs = scale*z
				var realPos = Vector3(xs,ys,zs) + origin
				var v = processPoint(realPos)/scale
				out_buffer.set_voxel_f(v,x,y,z,voxel_type)
					
					



# Configure

# Sample


func processPoint(pos:Vector3)->float:
	var a = GD_SDF.sphere(pos,radius)
	a += noise.get_noise_3d(pos.x,pos.y,pos.z) * noiseScale

	return a
