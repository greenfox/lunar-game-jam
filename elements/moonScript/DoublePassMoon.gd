#extends DoublePassStreamer
extends ScriptedDoublePassStreamer

class_name DoublePassMoon

export (float) var radius:float = Globals.planetRadius
export (float) var crustScale = .25 setget setCrustSize

export (float) var threshold = .5

var seaLevel:float = 0

var crustSize:float #= radius*0 setget setCrustSize
var minAltitude:float# = radius-crustSize
var maxAltitude:float# = radius+crustSize

export (float) var noiseScale:float = 2 setget setNoiseScale
export (OpenSimplexNoise) var noise:OpenSimplexNoise
var trueNoiseScale = null

func _init():
	pass
func _ready():
	pass


var solidChunks:int = 0
var emptyChunks:int = 0
var generatedChunks:int = 0
var countLock :=Mutex.new()


func pass_one(out_buffer:VoxelBuffer, origin:Vector3, lod:int):
	var scale = pow(2,lod)
	var halfExtends = Vector3.ONE * scale * 8 #scale * EdgeSize / 2
	var center = origin + halfExtends
	var boxHalfExtends = halfExtends
	
	if GD_SDF.roundBox(-center,boxHalfExtends,maxAltitude) > 0:
		countLock.lock()
		emptyChunks +=1
		countLock.unlock()
		return false#chunk is completely outside the planet
	if GD_SDF.roundBox(-center,boxHalfExtends,minAltitude-(2*boxHalfExtends.length())) < 0:
		countLock.lock()
		solidChunks +=1
		countLock.unlock()
		out_buffer.fill_f(-1,1) #this chunk is completely INSIDE the planet
		return false
	
	countLock.lock()
	generatedChunks +=1
	countLock.unlock()
	return true


func pass_two(position:Vector3)->float:
	var v = GD_SDF.sphere(position,radius)
	var sphereNormal = position.normalized()*radius
	var noiseValue = noise.get_noise_3dv(sphereNormal/trueNoiseScale)
	var correctedNoise = min(ridge(noiseValue),threshold)#(min(threshold,ridge(noiseValue)) - threshold) * (1-threshold)
	v += correctedNoise * crustSize
#	print(noiseValue,":",correctedNoise)
	return v


func ridge(value):
	return pow(1-abs(value),4)


func errorChunk(out_buffer,origin,lod):
	var scale = pow(2,lod)
	var halfScale = scale/2
	var center = origin + Vector3(halfScale,halfScale,halfScale)
	print("=========ERROR========= at LOD:", lod)
	print("center:",center)
	print("scale:",scale)
	print("GD_SDF test:",GD_SDF.box(-center,Vector3(scale,scale,scale)))


func setCrustSize(value):
	crustScale = value
	crustSize = crustScale * radius
	minAltitude = radius-crustSize #idk if thiswill work 
	maxAltitude = radius#+crustSize
	seaLevel = radius - (threshold * crustSize)


func setNoiseScale(value):
	noiseScale = value
	trueNoiseScale = (noiseScale * radius) / 1000  


func getChunkCounts():
	countLock.lock()
	var output = {
		solidChunks=solidChunks,
		emptyChunks=emptyChunks,
		generatedChunks=generatedChunks,
		total=solidChunks+emptyChunks+generatedChunks
	}
	countLock.unlock()
	return output
