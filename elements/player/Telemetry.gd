extends CanvasLayer

export (NodePath) var horzVel
export (NodePath) var vertVel
export (NodePath) var velocityAngle
export (NodePath) var shipAngle
export (NodePath) var targetAngle
export (NodePath) var fuelTank
func _ready():
	pass


func _on_PlayerController_telemetry(telemetry:TelemetryContainer):
#	var telemetryData = telemetry.toOverlay()
	get_node(horzVel).text = str(floor(telemetry.horizontalVelocity/Globals.scale)) + "m/s horzV"
	get_node(vertVel).text = str(floor(telemetry.verticalVelocity/Globals.scale)) + "m/s vertV"

	if telemetry.horizontalVelocity > 0.5:
		get_node(velocityAngle).visible = true
		get_node(velocityAngle).rect_rotation = telemetry.velocityAngle * 180/PI
	else:
		get_node(velocityAngle).visible = false

	get_node(shipAngle).rect_rotation = telemetry.shipAngle * 180/PI
	get_node(targetAngle).rect_rotation = telemetry.targetAngle * 180/PI

	get_node(fuelTank).value = (100*telemetry.fuel)/telemetry.fuelTank

	if has_node("DebugVector"):
		$DebugVector.transform.origin = telemetry.shipTransform.origin
		$DebugVector/shipVector.cast_to = telemetry.flattenVector(telemetry.shipVector).normalized() * 10
		$DebugVector/cameraVector.cast_to = telemetry.flattenVector(telemetry.cameraLook).normalized() * 10
		$DebugVector/shipVelocity.cast_to = telemetry.flattenVector(telemetry.shipVelocity).normalized() * 10

