class_name ControlState

var cameraMove: = Vector3.ZERO
var playerMove: = Vector3.ZERO

enum CONTROL_MODE {ROCKET,TRACKBALL,FLIGHT}
#enum CAMERA_MODE {NORMAL,ZOOM}

var controlMode = CONTROL_MODE.FLIGHT

var thrust:float = 0.0

const stringFormat = \
"""cameraMove={cameraMove}
playerMove={playerMove}
thrust={thrust}"""

func fix():
	var targetMax = 1.0
	if playerMove.length_squared() > targetMax:
		playerMove = playerMove.normalized() * targetMax
#	if cameraMove.length_squared() > targetMax:
#		cameraMove = cameraMove.normalized() * targetMax

	thrust = min(thrust,1.0)

func _to_string():
	breakpoint
	return stringFormat.format({
		cameraMove=cameraMove,
		playerMove=playerMove,
		thrust=thrust
	})

