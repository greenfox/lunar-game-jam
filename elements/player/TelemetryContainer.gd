class_name TelemetryContainer

#external
var shipVelocity:Vector3
var shipTransform:Transform
var cameraTransform:Transform
var planetCore:Vector3
var targetTransform:Transform
#internal
var upVector:Vector3

var verticalVector:Vector3
var verticalVelocity:float

var horizontalVector:Vector3
var horizontalVelocity:float

var velocityAngle#:float
var shipAngle#:float
var targetAngle

var shipVector:Vector3
var cameraLook:Vector3

var fuelTank:float
var fuel:float

func populateFromController(controller:Node) -> void:
	shipVelocity = controller.get_parent().linear_velocity
	shipTransform = controller.get_parent().global_transform
	fuelTank = controller.get_parent().fuelTank
	fuel = controller.get_parent().fuel
	cameraTransform = controller.get_node("CameraGimble/Camera").global_transform
	planetCore = Vector3.ZERO
	targetTransform = controller.get_tree().get_nodes_in_group("target")[0].global_transform
	recalculateValues()

func recalculateValues() ->void:
	upVector = (shipTransform.origin - planetCore).normalized()
	verticalVector = shipVelocity.project(upVector)
	
	if (verticalVector + shipTransform.origin).length_squared() > shipTransform.origin.length_squared():
		verticalVelocity = verticalVector.length()
	else:
		verticalVelocity = -verticalVector.length()
		
	horizontalVector = (shipVelocity - verticalVector)
	horizontalVelocity = horizontalVector.length()
	
	
	cameraLook = cameraTransform.basis*Vector3.FORWARD
	
	velocityAngle = getAngle(shipVelocity)
	shipVector = (shipTransform.basis * Vector3(0,1,0)) #- (shipTransform * Vector3(0,0,0))
	shipAngle = getAngle(shipVector)

	targetAngle = getAngle(targetTransform.origin)

func getAngle(vector:Vector3):
	var flatShipVect:Vector3 = flattenVector(vector)
	var flatCameraVect:Vector3 = flattenVector(cameraLook)
	if flatShipVect.dot(flatCameraVect.rotated(upVector,PI/2)) > 0:
		return -flatShipVect.angle_to(flatCameraVect)# * 180/PI
	else:
		return flatShipVect.angle_to(flatCameraVect)# * 180/PI

func flattenVector(vector:Vector3):
	return vector - vector.project(upVector)

