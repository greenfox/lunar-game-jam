extends Node

class_name PlayerController

onready var myCamera = $Camera #this will migrate around the scene

export (float) var mouseSensitivity := .5

var myControls:= ControlState.new()

var rStickState := Vector2.ZERO
var lStickState := Vector2.ZERO
var bumperHeld :=false

var thrusterState:float = 0.0
var throttleRate:float = 2.0

var node_reference:Node

var wheelEvents := 0.0

signal switchingControl
signal setControls
signal changeActiveViewer
signal telemetry

func _physics_process(delta):
	
	captureEvents(delta)
	myControls.thrust = thrusterState

	myControls.cameraMove += Vector3(rStickState.x,0,rStickState.y) if Input.is_action_pressed("alternateControls") else Vector3(rStickState.x,rStickState.y,0) 
	myControls.cameraMove += Vector3(0,0,wheelEvents)
	wheelEvents = 0
	myControls.playerMove += Vector3(0,lStickState.y,-lStickState.x) if Input.is_action_pressed("alternateControls") else Vector3(lStickState.x,lStickState.y,0)
	
	
	myControls.fix()
	emit_signal("setControls",myControls)
#	node_reference.setControls(myControls)

	myControls = ControlState.new()
	collectTelemetry()

func collectTelemetry():
	var telemetry = TelemetryContainer.new()
	telemetry.populateFromController(self)
	emit_signal("telemetry",telemetry)
	

func captureEvents(delta):
	if Input.is_action_pressed("fore"):
		myControls.playerMove.y += 1
	if Input.is_action_pressed("back"):
		myControls.playerMove.y -= 1
	if Input.is_action_pressed("left"):
		myControls.playerMove.x += 1
	if Input.is_action_pressed("right"):
		myControls.playerMove.x -= 1
	if Input.is_action_pressed("rollLeft"):
		myControls.playerMove.z -= 1
	if Input.is_action_pressed("rollRight"):
		myControls.playerMove.z += 1

	if Input.is_action_pressed("thrustUp"):
		thrusterState = min(1,thrusterState + (delta*throttleRate))
	if Input.is_action_pressed("thrustDown"):
		thrusterState = max(0,thrusterState - (delta*throttleRate))
	if Input.is_action_pressed("thrustMax"):
		thrusterState = 1
	if Input.is_action_pressed("thrustMin"):
		thrusterState = 0
#	if Input.is_action_just_pressed("zoomOut"):
#		pass
#	myControls.stablize = true if myControls.playerMove.length_squared() != 0 else false#Input.is_action_pressed("stablize")

func _input(event):
	#camera control
	"""
	Right stick is always camera control. Hold right click on the mouse is always camera control
	"""
	if event is InputEventMouseButton and event.button_index == 2:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if event.pressed else Input.MOUSE_MODE_VISIBLE)
	elif event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		myControls.cameraMove -= Vector3(event.relative.x,event.relative.y,0)*mouseSensitivity
	elif event is InputEventJoypadMotion:
		match event.axis:
			ControlConsts.axisRT:
				thrusterState = (event.axis_value+1)*0.5
			ControlConsts.axisLX:
				lStickState.x = -event.axis_value
			ControlConsts.axisLY:
				lStickState.y = -event.axis_value
			ControlConsts.axisRX:
				rStickState.x = event.axis_value
			ControlConsts.axisRY:
				rStickState.y = event.axis_value
	elif event is InputEventJoypadButton:
		if event.button_index == ControlConsts.LB or event.button_index == ControlConsts.RB:
			pass
	elif event is InputEventMouseButton and (event.button_index == 4 or event.button_index == 5):
		if event.button_index == 4:
			wheelEvents -= 1
		elif event.button_index == 5:
			wheelEvents += 1
	else:
		pass
#		if event is InputEventJoypadButton:
#			print("button index",event.button_index)
	
#func setTarget(value):
#	targetPath = value
#	target = get_node(value)


func _on_PlayerController_tree_entered():
	connect("setControls",get_parent(),"setControls")
	get_parent().connect("setCamera",self,"setCamera")
	emit_signal("changeActiveViewer",get_parent())
	


func _on_PlayerController_tree_exiting():
	disconnect("setControls",get_parent(),"setControls")
	get_parent().disconnect("setCamera",self,"setCamera")



