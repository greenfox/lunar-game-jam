# Links

[Pages Link](https://greenfox.gitlab.io/lunar-game-jam/)

[Itch.io Link ("stable" release)](https://greenf0x.itch.io/lunar-game-jam)



# TODO

- ~~Score Target~~
- ~~Angle Indicator~~
- ~~thrust image~~
- ~~sound~~
- ~~pause menu (with controls)~~
- ~~reset~~
- death
- timer points
- altimeter
- ~~rethink fuel~~
- speed warning
- shaders
- windows controllers
- orb staging
- smoke/light color
- fonts
- fix windows controller

# Wish List

- Delivery game (you have a cargo and each landing pad will pay a differently for it)
- Dust mots to indicate speed/direction
- Meta UI, move the compass to the game world so you don't have to look at your navBall
