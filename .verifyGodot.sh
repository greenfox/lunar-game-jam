#!/bin/bash

main()
{
    mkdir -p .bin
    URL="https://greenfox.gitlab.io/godotvoxel-ci-compile/"
    cat .binsManafest | readManafest $URL

    pushd .bin #this fixes a bug in zip archives
        mv $(find) . 
    popd
}

verify()
{
    if [ "$(md5sum $2 | awk '{print $1}')" = "$1" ]
    then
        echo "Valid!: $(basename $2)"
    else
        newURL="$URL/$(basename $2).zip"
        echo "Reaquiring!: $(basename $2) from $newURL"
        aquire "$newURL" "$2"
    fi
}

aquire()
{   
    wget -O $2.zip $1
    unzip -o $2.zip -d $(dirname $2)
    rm $2.zip
}

readManafest()
{
    while read line
    do
        verify $line $1
    done
}

main